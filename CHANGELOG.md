## Ports v1.2a2

- 01.08.2022 linuxoid85
    - Актуализация сведений в `CHANGELOG.md`;

- 01.08.2022 nordic_dev
    - Добавление порта `base/gawk`;

- 30.07.2022 nordic_dev
    - Добавление порта `base/flex`;
    - Добавление порта `base/findutils`;
    - Добавление порта `base/file`;
    - Добавление порта `base/expat`;
    - Добавление порта `base/eudev`;

- 29.07.2022 nordic_dev
    - Добавление порта `base/grep`;
    - Добавление порта `base/grep`;
    - Мелкий фикс `port.toml` порта `base/check`;
    - Добавление порта `base/check`;
    - Добавление порта `base/bison`;
    - Добавление порта `base/bc`;

- 28.07.2022 nordic_dev
    - Добавление порта `base/automake`;
    - Добавление порта `base/bash`;
    - Добавление порта `base/autoconf`;
    - Добавление порта `base/attr`;

- 27.07.2022 nordic_dev
    - Fix CR Issues (`base/cmake`);
    - Добавление порта `base/xz`;
    - Обновление `base/cmake` 3.22.2 -> 3.23.2;
    - Добавлены ссылки "Далее", "Домой", "Назад" в страницы документации;

- 27.07.2022 linuxoid85
    - Исправление [#50](https://gitlab.com/calmiralinux/cabs/Ports/-/issues/50);
    - Добавление порта `toolchain/bash`;
    - Изменение прав доступа к файлам `install` в директориях портов;
    - Добавление `files.list` в `wayland/wayland_protocols`;
    - Обновление `wayland/wayland` 1.20.0 -> 1.21.0;
    - Добавление порта `wayland/wayland_protocols`;

- 26.07.2022 nordic_dev
    - Minor inprovements in the port. Issue [#28](https://gitlab.com/calmiralinux/cabs/Ports/-/issues/28)
    - Добавление порта `general/fontconfig`;

- 26.07.2022 linuxoid85
    - Всевозможные исправления README: добавлены сведения о категориях
      `cross_compiler` и `toolchain`, файлы `description` и `index.html` портов
      переведены в разряд устаревших;
    - Актуализация сборочных инструкций кросс-компилятора;
    - [DOC] Актуализация сведений о требуемом ПО для создания порта;
    - Исправление [#45](https://gitlab.com/calmiralinux/cabs/Ports/-/issues/45);
    - [DOC] Дополнение сведений о предназначении параметров в файле `port.toml`
      порта;
    - [DOC] Обновление `docs/README.md`;

- 25.07.2022 linuxoid85
    - Добавление порта `toolchain/ncurses`;
    - Добавление порта `toolchain/m4`;
    - Добавление категории `toolchain`;
    - Добавление порта `cross_compiler/libstdcxx`;
    - Добавление порта `cross_compiler/glibc`;
    - Добавление порта `cross_compiler/linux`;
    - Добавление порта `cross_compiler/gcc`;
    - Добавление порта `cross_compiler/binutils`;
    - Добавление категории `cross_compiler`;
    - Добавление начальной поддержки cport;
    - Добавление порта `general/fontconfig`;

- 23.07.2022 linuxoid85
    - Обновление документации;
    - Добавление документации о создании порта;

- 21.07.2022 linuxoid85
    - Ограничение количества потоков для сборки `editors/gvim`: из-за ошибок
      сборки пакет собирается **только** в один поток, в начале сборки
      выводится об этом предупреждение;
    - Добавление порта `xorg/x11-drivers/libvdpau`;
    - Добавление категории `xorg/x11-drivers`;
    - Добавление порта `postcpl/libpwquality`;
    - Добавление порта `postcpl/linux-pam`;
    - Добавление порта `general/libpng`;
    - Добавление порта `general/pixman`;
    - Добавление порта `xorg/x11-minimal/xlegacy`;
    - Добавление порта `xorg/x11-minimal/xinit`;
    - Добавление порта `xorg/x11-minimal/xclock`;

- 19.07.2022 linuxoid85
    - Добавление порта `xorg/x11-minimal/xterm`;
    - Добавление порта `xorg/x11-minimal/xorg-server`;
    - Добавление порта `xorg/x11-minimal/xwayland`;
    - Добавление порта `xorg/x11-minimal/xkeyboard-config`;
    - Добавление порта `xorg/x11-minimal/xfonts`;
    - Добавление порта `xorg/x11-minimal/xcursor-themes`;
    - Добавление порта `xorg/x11-minimal/xapps`;
    - Добавление порта `xorg/x11-minimal/xbitmaps`;

- 18.07.2022 linuxoid85
    - Добавление порта `xorg/x11-minimal/mesa`;
    - Добавление порта `xorg/x11-minimal/xcb-util-cursor`;
    - Добавление порта `xorg/x11-minimal/xcb-util-wm`;
    - Добавление порта `general/llvm`;
    - Добавление порта `general/doxygen`;
    - Добавление порта `xorg/x11-minimal/xcb-util-renderutil`;
    - Добавление порта `xorg/x11-minimal/xcb-util-keysyms`;
    - Добавление порта `xorg/x11-minimal/xcb-util-image`;
    - Добавление порта `xorg/x11-minimal/xcb-util`;
    - Добавление порта `xorg/x11-libs/libxcvt`;
    - Добавление порта `xorg/x11-libs/xlibs`;
    - Добавление порта `xorg/x11-libs/libxcb`;

- 13.07.2022 linuxoid85
    - Добавление порта `xorg/x11-libs/libXdmcp`;
    - Добавление порта `xorg/x11-libs/libXau`;

- 04.07.2022 linuxoid85
    - Добавление порта `general/icu`;
    - Добавление порта `general/libxml2`;
    - Добавление порта `wayland/wayland`;
    - Добавление категории `wayland`;
    - Добавление порта `editors/gvim`;
    - Добавление категори `editors`;

- 03.07.2022 linuxoid85
    - Добавление портов `base/binutils`, `base/acl`;

- 29.06.2022 linuxoid85
    - Добавление порта `base/libuv`;
    - Добавление порта `base/cmake`;
    - Начало работы над сборкой архивов системы портов;
    - Добавление порта `postcpl/nodejs`;
    - Добавление категории `general`;
        - Добавление порта `general/which_orig`;
        - Добавление порта `general/which_script`;
    - Добавление порта `base/sys_configs`;
    - Добавление порта `postcpl/ntfs-3g`;
    - Добавление порта `postcpl/dosfstools`;
    - Добавление порта `postcpl/fuse`;

- 28.06.2022 linuxoid85
    - Добавление новых портов:
        - `postcpl/*`;
        - `xorg/x11-minimal/XBE`;
        - `xorg/x11-minimal/util-macros`;
        - `xorg/x11-minimal/xorgproto`;

- 31.05.2022 linuxoid85
    - Initial commit
        - Начало работы над новым стандартом системы портов, пригодным для дальнейшей модификации, расширения и совершенствования.
