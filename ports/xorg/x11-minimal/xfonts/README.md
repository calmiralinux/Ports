# xfonts-0.1

Some scalable fonts and supporting packages for Xorg applications. Many people will want to install other TTF or OTF fonts in addition to or instead of these

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 8.8 Mb

### Dependencies

- **required:**  xorg/x11-minimal/xcursor-themes
- **recommend:** none
- **optional:** none
- **conflict:** none

