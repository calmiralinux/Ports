#!/bin/bash -e
# Build script for 'mesa' package
# Copyright (C) 2021, 2022 Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>

NAME="mesa"
VERSION="21.3.6"

echo -e -n "\a\e[1mDo you use the NVIDIA GPU? [y/n]\e[0m "
read run

if [ "$run" == "Y" || "$run" == "y" ]; then
    cd /usr/src
    wget https://www.linuxfromscratch.org/patches/blfs/11.1/mesa-$VERSION-nouveau_fixes-1.patch
    MESA_PATCH="ok"
fi

cd /usr/src/"$NAME-$VERSION"

if [ "$MESA_PATCH" == "ok" ]; then
    if [ -f "../mesa-$VERSION-add_xdemos-1.patch" ]; then
        patch -Np1 -i ../mesa-$VERSION-nouveau_fixes-1.patch
    fi
fi

source /usr/ports/xorg/x11-minimal/mesa/port-configuration.sh

mkdir -v build
cd build

meson --prefix=$XORG_PREFIX          \
      --buildtype=release            \
      -Ddri-drivers=$DRI_DRIVERS     \
      -Dgallium-drivers=$GALLIUM_DRV \
      -Dgallium-nine=false           \
      -Dglx=dri                      \
      -Dvalgrind=disabled            \
      -Dlibunwind=disabled           \
      ..
unset GALLIUM_DRV DRI_DRIVERS

ninja
ninja install
