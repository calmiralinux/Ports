# mesa-21.3.6

Mesa is an OpenGL compatible 3D graphics library

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 756 Mb

### Dependencies

- **required:** xorg/x11-libs/xlibs xorg/x11-libs/libdrm general/pymo/mako general/llvm xorg/x11-drivers/libva wayland/wayland-protocols
- **recommend:**  xorg/x11-drivers/libvdpau
- **optional:** none
- **conflict:** none

