# xcb-proto-1.14.1

XML-XCB protocol descriptions that libxcb uses to generate the majority of its code and API

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1.2 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:**  general/libxml2
- **conflict:** none

