# xapps-0.1

Expected applications available in previous X Window implementations

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 48 Mb

### Dependencies

- **required:** general/libpng xorg/x11-minimal/mesa xorg/x11-minimal/xbitmaps xorg/x11-minimal/xcb-util
- **recommend:** none
- **optional:**  postcpl/linux-pam
- **conflict:** none

