# xcb-util-keysyms-0.4.0

Library for handling standard X key constants and conversion to/from keycodes

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2.2 Mb

### Dependencies

- **required:**  xorg/x11-libs/libxcb
- **recommend:** none
- **optional:**  general/doxygen
- **conflict:** none

