# libXau-1.0.9

Library implementing the X11 Authorization Protocol.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0' 'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2.8 Mb

### Dependencies

- **required:**  xorg/x11-minimal/xorgproto
- **recommend:** none
- **optional:** none
- **conflict:** none

