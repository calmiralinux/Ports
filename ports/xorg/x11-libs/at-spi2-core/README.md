# at-spi2-core-2.44.1

At-Spi2 Core package is a part of the GNOME Accessibility Project. It provides a Service Provider Interface for the Assistive Technologies available on the GNOME platform and a library against which applications can be linked

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 5.5 Mb

### Dependencies

- **required:**  general/dbus general/glib xorg/x11-libs/xlibs
- **recommend:** none
- **optional:**  general/gtk-doc general/gobject-introspection
- **conflict:** none

