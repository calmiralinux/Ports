# xmlto-0.0.28

The xmlto package is a front-end to a XSL toolchain

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1.5 Mb

### Dependencies

- **required:**   pst/docbook-xml-4.5 pst/dokbook-xsl general/libxslt
- **recommend:** none
- **optional:** pst/fop pst/dblatex pst/passivetex net/links
- **conflict:** none

