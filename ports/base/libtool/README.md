# libtool-2.4.7

The Libtool package contains the GNU generic library support script

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 14 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gawk base/gcc base/glibc base/grep base/make base/autoconf base/automake base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

