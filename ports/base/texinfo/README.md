# texinfo-6.8

The Texinfo package contains programs for reading writing and converting info pages

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 113 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/ncurses base/make base/patch base/sed base/gettext
- **recommend:** none
- **optional:** none
- **conflict:** none

