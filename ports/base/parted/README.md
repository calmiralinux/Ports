# parted-3.5

The Parted package is a disk partitioning and partition resizing tool

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 35 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** general/dosfstools general/pth pst/texlive
- **conflict:** none

