# cport-1.0

Ports manager for Calmira 2.0+ GNU/Linux-libre

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 0.5 Mb

### Dependencies

- **required:** base/python3 base/pymo/wget base/bash base/sqlite3
- **recommend:** none
- **optional:** none
- **conflict:** none

