# wheel-0.37.1

Wheel is a Python library that is the reference implementation of the Python wheel packaging standard

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1 Mb

### Dependencies

- **required:**   base/python 
- **recommend:** none
- **optional:** none
- **conflict:** none

