# wget-1.21.1

The Wget package contains a utility useful for non-interactive downloading of files from the Web

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 45 Mb

### Dependencies

- **required:** bae/make-ca base/openssl base/pcre net/libpsl
- **recommend:** none
- **optional:** general/libidn2
- **conflict:** none

