# autoconf-2.71

Autoconf is an extensible package of M4 macros that produce shell scripts to automatically configure software source code packages.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 14 Mb

### Dependencies

- **required:** base/bash base/coreutils base/grep base/m4 base/make base/perl base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

