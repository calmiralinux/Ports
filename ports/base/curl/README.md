# curl-7.85.0

The cURL package contains an utility and a library used for transferring files with URL syntax to any of the following protocols: DICT FILE FTP FTPS GOPHER GOPHERS HTTP HTTPS IMAP IMAPS LDAP LDAPS MQTT POP3 POP3S RTSP SMB SMBS SMTP SMPTS TELNET and TFTP. Its ability to both download and upload files can be incorporated into other programs to support functions like streaming media

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 144.0 Mb

### Dependencies

- **required:** none
- **recommend:**  base/make-ca
- **optional:**  general/brotli
- **conflict:** none

