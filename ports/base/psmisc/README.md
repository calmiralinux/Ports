# psmisc-23.5

The Psmisc package contains programs for displaying information about running processes

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 8 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/grep base/ncurses base/make base/grep base/sed
- **recommend:** none
- **optional:** none
- **conflict:** none

