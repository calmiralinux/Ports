# less-590

The Less package contains a text file viewer.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 5 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gcc base/glibc base/grep base/ncurses base/make base/sed base/gzip
- **recommend:** none
- **optional:**   general/pcre 
- **conflict:** none

