# pkg-config-0.29.2

The pkg-config package contains a tool for passing the include path and/or library paths to build tools during the configure and make phases of package installations

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 31 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gawk base/gcc base/glibc base/make base/grep base/popt base/sed base/kmod
- **recommend:** none
- **optional:**   general/glib 
- **conflict:** none

