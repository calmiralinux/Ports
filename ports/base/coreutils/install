#!/bin/bash -e
# Build script for 'coreutils' package
# Copyright (C) 2021, 2022 Sergey Gaberer <nordic.dev@pm.me>

NAME="coreutils"
VERSION="9.1"

cd /usr/src
wget https://www.linuxfromscratch.org/patches/lfs/development/coreutils-9.1-i18n-1.patch

cd /usr/src/"$NAME-$VERSION"

patch -Np1 -i ../coreutils-9.1-i18n-1.patch

autoreconf -fiv
FORCE_UNSAFE_CONFIGURE=1 ./configure \
            --prefix=/usr            \
            --enable-no-install-program=kill,uptime

make
make install

# Move programs to the locations specified by the FHS
mv -v /usr/bin/chroot /usr/sbin
mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/' /usr/share/man/man8/chroot.8
mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
mv -v /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
mv -v /usr/bin/{rmdir,stty,sync,true,uname} /bin
mv -v /usr/bin/{head,nice,sleep,touch} /bin
