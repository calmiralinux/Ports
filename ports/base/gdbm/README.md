# gdbm-1.23

GNU dbm (or GDBM for short) is a library of database functions that use extensible hashing and work similar to the standard UNIX dbm.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 14 Mb

### Dependencies

- **required:** base/bash base/binutils base/gcc base/glibc base/make
- **recommend:**  base/readline
- **optional:** none
- **conflict:** none

