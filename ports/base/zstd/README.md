# zstd-1.5.2

Zstandard is a real-time compression algorithm providing high compression ratios. It offers a very wide range of compression / speed trade-offs while being backed by a very fast decoder

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 55 Mb

### Dependencies

- **required:** base/binutils base/coreutils base/gcc base/glibc base/gzip base/make base/patch base/xz
- **recommend:** none
- **optional:**  general/lz4
- **conflict:** none

