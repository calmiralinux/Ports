# gperf-3.1

Gperf generates a perfect hash function from a key set

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 7 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/make
- **recommend:** none
- **optional:** none
- **conflict:** none

