# gmp-6.2.1

The GMP package contains math libraries. These have useful functions for arbitrary precision arithmetic.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 36 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gawk base/gcc base/glibc base/grep base/m4 base/make base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

