# bzip2-1.0.8

The Bzip2 package contains programs for compressing and decompressing files

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 9 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gcc base/glibc base/make base/patch base/file
- **recommend:** none
- **optional:** none
- **conflict:** none

