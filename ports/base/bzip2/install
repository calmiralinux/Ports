#!/bin/bash -e
# Build script for 'bzip2' package
# Copyright (C) 2021, 2022 Sergey Gaberer <nordic.dev@pm.me>

NAME="bzip2"
VERSION="1.0.8"

cd /usr/src
wget https://www.linuxfromscratch.org/patches/lfs/development/bzip2-1.0.8-install_docs-1.patch

cd /usr/src/"$NAME-$VERSION"

patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch

# Ensure installation of symbolic links are relative
sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile

# Ensure the man pages are installed into the correct location
sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile

# Create a dynamic libbz2.so library and links the Bzip2 utilities against it
make -f Makefile-libbz2_so
make clean

make
make PREFIX=/usr install

# Install the shared library
cp -av libbz2.so.* /usr/lib
ln -sv libbz2.so.$VERSION /usr/lib/libbz2.so

cp -v bzip2-shared /usr/bin/bzip2
for i in /usr/bin/{bzcat,bunzip2}; do
  ln -sfv bzip2 $i
done

# Remove a useless static library
rm -fv /usr/lib/libbz2.a