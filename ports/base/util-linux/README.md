# util-linux-2.38

Miscellaneous utility programs. Among them are utilities for handling file systems consoles partitions and messages

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 261 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/eudev base/findutils base/gawk base/gcc base/gettext base/glibc base/grep base/libcap base/make base/ncurses base/sed base/zlib
- **recommend:** none
- **optional:**  postcpl/linux-pam postcpl/smartmontools
- **conflict:** none

