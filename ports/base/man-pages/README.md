# man-pages-5.13

Over 2_2000 manual pages

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 33 Mb

### Dependencies

- **required:**  base/bash base/coreutils base/make
- **recommend:** none
- **optional:** none
- **conflict:** none

