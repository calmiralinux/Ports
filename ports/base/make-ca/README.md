# make-ca-1.10

Utility to deliver and manage a complete PKI configuration

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 6.6 Mb

### Dependencies

- **required:**  base/p11-kit # p11-kit-0.24.1
- **recommend:** none
- **optional:**  postcpl/nss general/fcron # nss-3.75 fcron-3.2.1
- **conflict:** none

