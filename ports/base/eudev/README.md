# eudev-3.2.11

Programs for dynamic creation of device nodes

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 83 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gawk base/gcc base/glibc base/grep base/gperf base/make base/sed base/util-linux
- **recommend:** none
- **optional:** none
- **conflict:** none

