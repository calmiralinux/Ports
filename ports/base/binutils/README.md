# binutils-2.38

The Binutils package contains a linker an assembler and other tools for handling object files

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 1218 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/file base/flex base/flex base/gawk base/gcc base/glibc base/grep base/make base/perl base/sed base/texinfo base/zlib
- **recommend:** none
- **optional:** none
- **conflict:** none

