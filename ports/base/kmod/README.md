# kmod-30

The Kmod package contains libraries and utilities for loading kernel modules

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 12 Mb

### Dependencies

- **required:** base/bash base/binutils base/bison base/coreutils base/flex base/gcc base/gettext base/glibc base/gzip base/make base/openssl base/pkg-config base/sed base/xz base/zlib base/eudev
- **recommend:** none
- **optional:** none
- **conflict:** none

