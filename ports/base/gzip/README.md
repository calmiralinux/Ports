# gzip-1.12

The Gzip package contains programs for compressing and decompressing files

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 20 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/grep base/make base/sed base/texinfo base/man-db
- **recommend:** none
- **optional:** none
- **conflict:** none

