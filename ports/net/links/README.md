# links-2.27

Text and graphics mode WWW browser. It includes support for rendering tables and frames features background downloads can display colors and has many other features

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 37.5 Mb

### Dependencies

- **required:** none
- **recommend:**  net/libevent
- **optional:**  general/gpm general/libpng general/libjpeg
- **conflict:** none

