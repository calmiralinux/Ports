# libpsl-0.21.1

The libpsl package provides a library for accessing and resolving information from the Public Suffix List (PSL)

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 62 Mb

### Dependencies

- **required:** general/libidn2
- **recommend:** none
- **optional:** general/gtk-doc
- **conflict:** none

