# dosfstools-4.2

The dosfstools package contains various utilities for use with the FAT family of file systems.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 3.5 Mb

### Dependencies

- **required:**  
- **recommend:**  
- **optional:**  
- **conflict:**  

