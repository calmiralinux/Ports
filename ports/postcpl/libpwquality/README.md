# libpwquality-1.4.4

Common functions for password quality checking and also scoring them based on their apparent randomness. The library also provides a function for generating random passwords with good pronounceability

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 4.1 Mb

### Dependencies

- **required:**  postcpl/cracklib
- **recommend:**  postcpl/linux-pam
- **optional:** none
- **conflict:** none

