# wayland-1.22.0

Project to define a protocol for a compositor to talk to its clients as well as a library implementation of the protocol

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3' 'v2.0'
- **usage:** 6.2 Mb

### Dependencies

- **required:**  general/libxml base/expat base/libffi base/pkg-config base/cmake
- **recommend:** none
- **optional:**  general/doxygen
- **conflict:** none

