# glibc-2.36

The main C library. This library provides the basic routines for allocating memory searching directories opening and closing files reading and writing files string handling pattern matching arithmetic and so on

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 821.0 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** none
- **conflict:** none

