# libunistring-0.9.10

ibunistring is a library that provides functions for manipulating Unicode strings and for manipulating C strings according to the Unicode standard

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 51 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** pst/texlive-20210325
- **conflict:** none

