# graphviz-5.0.0

Graphviz is a package of open-source tools for drawing graphs specified in DOT language scripts having the filename extension \gv\.

## Detailed information
### Port/package

- **maintainer:** Repyakh Ivan <red-122@mail.ru>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 196.3 Mb

### Dependencies

- **required:**  base/make base/bash
- **recommend:**  general/libpng
- **optional:** none
- **conflict:** none

