# pixman-0.40.0

Library that provides low-level pixel manipulation features such as image compositing and trapezoid rasterization

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 55 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:**  xorg/x11-libs/gtk2 general/libpng
- **conflict:** none

