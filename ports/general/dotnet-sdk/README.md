# dotnet-sdk-6.0.401

.NET is a free cross-platform open-source developer platform for building many different types of applications

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 845 Mb

### Dependencies

- **required:** base/gcc base/glibc base/openssl base/zlib general/icu-67 general/krb5 general/libgdiplus
- **recommend:** none
- **optional:** none
- **conflict:** none

