# pcre-8.45

The PCRE package contains Perl Compatible Regular Expression libraries

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 25 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:**   general/valgrind 
- **conflict:** none

