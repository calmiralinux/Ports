# docbook-4.5

The DocBook-4.5 SGML DTD package contains document type definitions for verification of SGML data files against the DocBook rule set

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1 Mb

### Dependencies

- **required:**   pst/sgml-common general/unzip 
- **recommend:** none
- **optional:** none
- **conflict:** none

