# scdoc-1.11.2

scdoc is a simple man page generator for POSIX systems

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 3 Mb

### Dependencies

- **required:** base/make base/gcc base/glibc
- **recommend:** none
- **optional:** none
- **conflict:** none

