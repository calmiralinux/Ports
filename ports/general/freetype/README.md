# freetype-2.12.1

A library which allows applications to properly render TrueType fonts

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 31.0 Mb

### Dependencies

- **required:** none
- **recommend:**  general/harfbuzz general/libpng general/which_orig
- **optional:**  general/brotli
- **conflict:**  

