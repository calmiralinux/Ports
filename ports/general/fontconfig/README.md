# fontconfig-2.14.0

Fontconfig is a font configuration and customization library.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 4.8 Mb

### Dependencies

- **required:**  general/freetype general/gperf general/uuid general/libexpat1 general/pkg-config
- **recommend:**  pst/docbook
- **optional:**  pst/docbook-utils pst/texlive-formats-extra
- **conflict:** none

