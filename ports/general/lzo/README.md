# lzo-2.10

LZO is a data compression library which is suitable for data decompression and compression in real-time. This means it favors speed over compression ratio

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 12 Mb

### Dependencies

- **required:**  'base/make' 'base/glibc'
- **recommend:** none
- **optional:** none
- **conflict:** none

