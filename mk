#!/usr/bin/python3

import tarfile

def mkarch() -> None:

    with tarfile.open("./ports.txz", "w:xz") as f:
        f.add("./ports", recursive = True)
    
mkarch()
