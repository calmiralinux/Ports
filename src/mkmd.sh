#!/usr/bin/bash -e

# Copyright (C) 2022 Sergey Gaberer <nordic.dev@pm.me>

DIR="../ports/${1}"
FILE="${DIR}/port.toml"

NAME=
VERSION=
DESCRIPTION=
MAINTAINER=
RELEASES=()
USAGE=
REQUIRED=()
RECOMMEND=()
OPTIONAL=()
CONFLICT=()

ARRRAY_READING="none"

# is_file_exists()
#    Checks if the file exists.
# Return:
#    0 - File exists
#    1 - File not exists
is_file_exists() {
  [ -r "${1}" ]
}

# is_key_value()
#  Check if string contains key=value pair.
# Required variable:
#  0 - String to check
# Return:
#  0 - Key=value pair found
#  1 - No key=value pair found
is_key_value() {
  [[ "$1" == *=* ]]
}

# is_single_line_array()
#  Check if string is array.
# Required variable:
#  0 - String to check
# Return:
#  0 - Is array
#  1 - No array found
is_single_line_array() {
  [[ "$1" == *[* ]] && [[ "$1" == *]* ]]
}

# is_line_has_port()
#  Check if string contains port
# Required variable:
#  1 - Line to check
# Return:
#  0 - Contains port
#  1 - No port found
is_line_has_port() {
  [[ "$1" == */* ]]
}

# trim()
#  Remove whitespace.
# Required variable:
#  1 - String
# Return:
#  0 - String without whitespaces
trim() {
  echo $(echo "$1" | tr -d '[:space:]')
}

# filter()
#  Remove special characters.
# Required variable:
#  1 - String
# Return:
#  0 - String without special characters
filter() {
  echo $(echo "$1" | tr -d '",')
}

# filter_array()
#  Remove special characters.
# Required variable:
#  1 - String
# Return:
#  0 - String without special characters
filter_array() {
    values=$(echo "$1" | tr -d '[]=,"')
    echo "$values"
}

# Parse toml line
parse_line() {
  case $1 in
    "name")
      NAME=$(filter $(trim "$2"))
    ;;

    "version")
      VERSION=$(filter $(trim "${2}"))
    ;;

    "description")
      DESCRIPTION=$(filter "$2")
    ;;

    "maintainer")
      MAINTAINER=$(filter "$2")
    ;;

    "releases")
      if is_single_line_array "$2"; then
        RELEASES=$(filter_array "$2")
      else
        if is_line_has_port "$2"; then
          RELEASES+=("$2")
        fi
        ARRAY_READING="releases"
      fi
    ;;

    "usage")
      USAGE=$(filter $(trim "${2}"))
    ;;

    "required")
      if is_single_line_array "$2"; then
        REQUIRED=$(filter_array "$2")
      else
        if is_line_has_port "$2"; then
          REQUIRED+=("$2")
        fi
        ARRAY_READING="required"
      fi
    ;;

    "recommend")
      if is_single_line_array "$2"; then
        RECOMMEND=$(filter_array "$2")
      else
        if is_line_has_port "$2"; then
          RECOMMEND+=("$2")
        fi
        ARRAY_READING="recommend"
      fi
    ;;

    "optional")
      if is_single_line_array "$2"; then
        OPTIONAL=$(filter_array "$2")
      else
        if is_line_has_port "$2"; then
          OPTIONAL+=("$2")
        fi
        ARRAY_READING="optional"
      fi
    ;;

    "conflict")
      if is_single_line_array "$2"; then
        CONFLICT=$(filter_array "$2")
      else
        if is_line_has_port "$2"; then
          CONFLICT+=("$2")
        fi
        ARRAY_READING="conflict"
      fi
    ;;

    *)
    ;;
  esac
}

# Parse array
read_array() {
  case "$ARRAY_READING" in
    "releases")
      if [[ "$1" == *]* ]]; then
        if [[ "$1" == */* ]]; then
          RELEASES+=("$1")
        fi
        ARRAY_READING="none"
      else
        RELEASES+=("$1")
      fi
    ;;

    "required")
      if [[ "$1" == *]* ]]; then
        if [[ "$1" == */* ]]; then
          REQUIRED+=("$1")
        fi
        ARRAY_READING="none"
      else
        REQUIRED+=("$1")
      fi
    ;;

    "recommend")
      if [[ "$1" == *]* ]]; then
        if [[ "$1" == */* ]]; then
          RECOMMEND+=("$1")
        fi
        ARRAY_READING="none"
      else
        RECOMMEND+=("$1")
      fi
    ;;

    "optional")
      if [[ "$1" == *]* ]]; then
        if [[ "$1" == */* ]]; then
          OPTIONAL+=("$1")
        fi
        ARRAY_READING="none"
      else
        OPTIONAL+=("$1")
      fi
    ;;

    "conflict")
      if [[ "$1" == *]* ]]; then
        if [[ "$1" == */* ]]; then
          CONFLICT+=("$1")
        fi
        ARRAY_READING="none"
      else
        CONFLICT+=("$1")
      fi
    ;;

    *)
      ARRAY_READING="none"
    ;;
  esac
}

generate_markdown() {
  MD_FILE="${DIR}/README.md"

  # Prevent append to the existing file
  if is_file_exists "${MD_FILE}"; then
    rm $MD_FILE
  fi
  echo "Generating ${MD_FILE}"
  echo "# ${NAME}-${VERSION}

${DESCRIPTION}

## Detailed information
### Port/package

- **maintainer:** ${MAINTAINER}
- **releases:** ${RELEASES[@]}
- **usage:** ${USAGE} Mb

### Dependencies

- **required:** ${REQUIRED[@]}
- **recommend:** ${RECOMMEND[@]}
- **optional:** ${OPTIONAL[@]}
- **conflict:** ${CONFLICT[@]}
" >> "${MD_FILE}"
}

run() {
  if ! is_file_exists "$FILE"; then
    echo "Could dot find port $FILE"
    exit 1
  fi

  echo "Reading ${FILE}"

  while IFS=, read -r line; do
    if is_key_value "$line"; then
      var=$(trim "${line%%=*}")
      value=${line##*=}

      parse_line "$var" "$value"
    else
      read_array $(filter "$line")
    fi
  done < "$FILE"

  if (( ${#REQUIRED[@]} == 0 )); then
    REQUIRED="none"
  fi

  if (( ${#RECOMMEND[@]} == 0 )); then
    RECOMMEND="none"
  fi

  if (( ${#OPTIONAL[@]} == 0 )); then
    OPTIONAL="none"
  fi

  if (( ${#CONFLICT[@]} == 0 )); then
    CONFLICT="none"
  fi

  generate_markdown

  echo "Done."

  exit 0
}


run
